# Mac Development Ansible Playbook

Borrowed heavily from - [geerlingguy - Mac Dev Playbook](https://github.com/geerlingguy/mac-dev-playbook)

Sets up a Macbook for development.

## Bootstrap

  1. Manually install applications from the list below (Install Apps Manually)
  2. Ensure Apple's command line tools are installed (`xcode-select --install` to launch the installer).
  3. Clone or download this repository to your local drive.
  4. Run the `bootstrap.sh` script.

> Note: If some Homebrew commands fail, you might need to agree to Xcode's license or fix some other Brew issue. Run `brew doctor` to see if this is the case.

## One time manual actions

### Install Apps Manually
  - Spotify (download)
  - Discord (download)
  - Telegram (appstore)

### Cleanup Finder
Open Finder window

  - [ ] Modify Sidebar
  	- Preferences (cmd-,) ->
		- General
			- New Finder Window Shows: home directory
		- Sidebar
			- Add Pictures folder
			- Add home directory
			- Remove Tags
  - [ ] Customize the Finder window header
	- In the Finder window header change the layout to "Columns"
	- Right Click on the Finder window header and select "Customize Toolbar..."
		- Add: Drag these items from the modal into the Finder window header 
			- Path
			- Airdrop
		- Remove: Drag these items from the Finder window header into the modal 
			- Tags
			- Share
			- Action

### SSH Key Setup

SSH Key setup is done via a separate tag in the playbook to simplify the scripts. Simply run:

```
ansible-playbook main.yml -t ssh
```

### Keyboard & trackpad settings

- [ ] Map CAPS_LOCK to CTRL: System Preferences -> Keyboard -> Modifier keys
	- Remember to do this for the host's keyboard and any keyboard attached.
- [ ] Desktop zooming: System Preferences -> Accessibility -> Zoom -> Use scroll gesture with modifier keys to zoom -> Check the box and choose CTRL in dropdown
- [ ] Tap to click: System Preferences -> Trackpad -> Select Tap to click

### Terminal 

- [ ] Change iTerm's font to SauceCodePro Nerd Font (found in ./roles/neovim/files)
- [ ] Change iTerm's colorscheme
	- Preferences (cmd-,) -> Profiles -> Colors -> Import... Browse to this projects dotfiles/files directory and import the tokyo night theme
- [ ] Change iTerm's config to understand the Option key as the Meta key sequence.
	- Preferences (cmd-,) -> Profiles -> Keys -> Left Option Key: `Esc+`

### Neovim First Run
Invoke nvim, bypass the error by hitting enter and let the package manager install packages. Exit and reopen

## Testing

[Tart](https://github.com/cirruslabs/tart) is used to test the operation of the 
ansible installation process. Run:

```bash
tart clone ghcr.io/cirruslabs/macos-sonoma-base:latest sonoma-base
tart clone sonoma-base mac-dev-playbook-test
tart run mac-dev-playbook-test --dir=mac-dev-playbook:../mac-dev-playbook
# Needed only to cleanup failed tests
# tart delete mac-dev-playbook-test
```

The project will be mounted at `/Volumes/My\ Shared\ Files/mac-dev-playbook/`


## TODO
- [ ] Locate the osx script and reintegrate it to this project or remove the ansible tasks for it.
- [ ] Cleanup the filenaming scheme in the dotfiles role
