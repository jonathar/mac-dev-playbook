local vim = vim
local fn = vim.fn

local execute = vim.api.nvim_command

-- ensure that packer is installed
local install_path = fn.stdpath('data')..'/site/pack/packer/opt/packer.nvim'
if fn.empty(fn.glob(install_path)) > 0 then
	-- keep these around for a bit
	-- execute('!git clone https://github.com/wbthomason/packer.nvim '..install_path)
	-- execute 'packadd packer.nvim'

	packer_bootstrap = fn.system({'git', 'clone', '--depth', '1', 'https://github.com/wbthomason/packer.nvim', install_path})
end

vim.cmd [[packadd packer.nvim]]


return require('packer').startup(function()
	use 'wbthomason/packer.nvim'

	-- LSP
	use {
		'neovim/nvim-lspconfig',
		requires = {
      -- Automatically install LSPs to stdpath for neovim
      'williamboman/mason.nvim',
      'williamboman/mason-lspconfig.nvim',

      -- Useful status updates for LSP
      'j-hui/fidget.nvim',

      -- Additional lua configuration, makes nvim stuff amazing
      'folke/neodev.nvim',
		},
	}

	-- Utilities
	use 'tpope/vim-surround'
	use 'tpope/vim-unimpaired'
	use 'tpope/vim-fugitive'
	use 'diepm/vim-rest-console'
	use 'preservim/nerdtree'
	use 'easymotion/vim-easymotion'	
	use 'tpope/vim-commentary'
	use 'tpope/vim-endwise'
	use 'jvirtanen/vim-hcl'
	use 'nvim-treesitter/nvim-treesitter'
	use { 'nvim-telescope/telescope.nvim', requires = { {'nvim-lua/plenary.nvim'} } }
	use 'ThePrimeagen/harpoon'

	-- TMUX plugins
	use 'christoomey/vim-tmux-navigator'
	use 'christoomey/vim-tmux-runner'

	-- Language Plugins
	use 'fatih/vim-go'
	use 'stephpy/vim-yaml'
	use 'uarun/vim-protobuf'

	-- Colorschemes
	use 'morhetz/gruvbox'
	use 'EdenEast/nightfox.nvim'
	use 'savq/melange'
	use 'sainnhe/sonokai'
	use 'folke/tokyonight.nvim'
	-- use 'vim-airline/vim-airline'
	-- use 'vim-airline/vim-airline-themes'
	use 'kyazdani42/nvim-web-devicons'
	use { 'nvim-lualine/lualine.nvim', requires = { 'kyazdani42/nvim-web-devicons', opt = true } }

	-- Completion
	use { 
		'hrsh7th/nvim-cmp',
		requires = { 'hrsh7th/cmp-nvim-lsp', 'L3MON4D3/LuaSnip', 'saadparwaiz1/cmp_luasnip' },
	}
	use 'hrsh7th/cmp-buffer'
	use 'hrsh7th/cmp-path'

	if packer_bootstrap then
		require('packer').sync()
	end
end)
