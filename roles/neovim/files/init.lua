require('plugins')
require('opts')
require('keymaps')
require('lsp')
require('lualine_opts')
require('treesitter_opts')
