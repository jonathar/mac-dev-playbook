#! /bin/bash

pip3 install --upgrade pip
pip3 install ansible

export PATH=$(python3 -m site --user-base)/bin:$PATH
ansible-galaxy install -r requirements.yml
ansible-playbook main.yml -t install-brew --ask-become-pass

eval $(/opt/homebrew/bin/brew shellenv)
ansible-playbook main.yml 
